<?php

namespace QRKey\Result;

use QRKey\IUser;
use QRKey\Auth;

abstract class AbstractResult implements IResult
{


    /**
     * @var IUser
     */
    protected $_user;

    /**
     * @var int
     */
    protected $_result;


    /**
     * AbstractResult constructor.
     * @param int   $result
     * @param IUser $user
     */
    public function __construct($result, $user = null)
    {
        $this->_result = $result;
        $this->_user   = $user;
    }


    /**
     * Получить данные ответа
     * @return array
     */
    abstract public function getResponse();


    /**
     * @return bool
     */
    public function isSuccess()
    {
        return $this->getResult() == Auth::SUCCESS;
    }


    /**
     * @return IUser
     */
    public function getUser()
    {
        return $this->_user;
    }


    /**
     * @return int
     */
    public function getResult()
    {
        return $this->_result;
    }

}
