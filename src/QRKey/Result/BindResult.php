<?php

namespace QRKey\Result;

/**
 * Результат привязки
 * Class BindResult
 * @package QRKey\Result
 */
class BindResult extends AbstractResult
{


    /**
     * @inheritdoc
     * @return array
     */
    public function getResponse()
    {
        $response = [
            'success' => $this->isSuccess(),
        ];

        if ($this->isSuccess()) {
            $response['login']      = $this->getUser()->getLogin();
            $response['nonce_type'] = $this->getUser()->getNonceType();
        }

        return $response;
    }


}