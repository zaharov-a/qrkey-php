<?php

namespace QRKey\Result;


/**
 * Результат авторизации
 * Class AuthResult
 * @package QRKey\Result
 */
class AuthResult extends AbstractResult
{


    /**
     * @inheritdoc
     * @return array
     */
    public function getResponse()
    {
        return [
            'success' => $this->isSuccess(),
        ];
    }

}