<?php

namespace QRKey\Result;


use QRKey\IUser;

interface IResult
{


    /**
     * IResult constructor.
     * @param int   $result
     * @param IUser $user
     */
    public function __construct($result, $user);


    /**
     * @return bool
     */
    public function isSuccess();


    /**
     * @return array
     */
    public function getResponse();

}