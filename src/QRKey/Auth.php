<?php

namespace QRKey;

use QRKey\Result\BindResult;
use QRKey\Result\AuthResult;
use QRKey\Result\IResult;

/**
 * Class Auth
 * @package QRKey
 */
class Auth
{


    const SUCCESS                    = 1;
    const FAILURE                    = 2;
    const FAILURE_IDENTITY_NOT_FOUND = 3;
    const FAILURE_CREDENTIAL_INVALID = 4;
    const FAILURE_IDENTITY_AMBIGUOUS = 5;


    /**
     * Пользователь
     * @var IUser
     */
    protected $_user;

    /**
     * Репозиторий пользователей
     * @var IRepository
     */
    protected $_repository;

    /**
     * Значение максимального рассинхронизации с ключом
     * @var int
     */
    protected $_maxDesync = 10;


    /**
     * Auth constructor.
     * @param IRepository $repository
     */
    public function __construct(IRepository $repository, $maxDesync = 10)
    {
        $this->_repository = $repository;
        $this->_maxDesync = $maxDesync;
    }


    /**
     * Установить значение максимальной рассинхронизации
     * @param int $maxDesync
     * @return Auth
     */
    public function setMaxDesync($maxDesync)
    {
        $this->_maxDesync = $maxDesync;
        return $this;
    }


    /**
     * Авторизовать
     * @param string $login
     * @param string $otp
     * @return AuthResult
     */
    public function authenticate($login, $otp)
    {
        $this->_user = $this->_repository->findByLogin($login);
        if (!$this->_user) {
            return new AuthResult(self::FAILURE_IDENTITY_NOT_FOUND);
        }

        if ($this->_user->isDesync()) {
            return new AuthResult(self::FAILURE_IDENTITY_AMBIGUOUS);
        }

        $nonce  = $this->_user->getNonce();
        $secret = $this->_user->getSecret();



        for ($i = -$this->_maxDesync; $i < $this->_maxDesync; $i++) {
            $newNonce = $nonce + $i;
//            error_log($newNonce . $secret);
            $hash = substr(sha1($newNonce . $secret), 0, 10);
//            error_log($hash);

            if ($otp === $hash) {
                $this->_user->setNonce($newNonce);
                return new AuthResult(self::SUCCESS, $this->getUser());
            }
        }

        $this->_user->markDesync();

        return new AuthResult(self::FAILURE_CREDENTIAL_INVALID);
    }


    /**
     * Привязать пользователя
     * @param string $token
     * @param string $secret
     * @return IResult
     */
    public function bind($token, $secret)
    {
        $this->_user = $this->_repository->findByToken($token);
        if (!$this->_user) {
            return new BindResult(self::FAILURE_IDENTITY_NOT_FOUND);
        }

        $this->_user->setSecret($secret);
        $this->_user->setNonce(0);

        return new BindResult(self::SUCCESS, $this->getUser());
    }


    /**
     * Получить пользователя
     * @return IUser
     */
    public function getUser()
    {
        return $this->_user;
    }
}