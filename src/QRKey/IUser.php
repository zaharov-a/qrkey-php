<?php

namespace QRKey;


/**
 * Интерфейс пользователя
 * Interface IUser
 * @package QRKey
 */
interface IUser
{


    /**
     * Получить логин
     * @return int|string
     */
    public function getLogin();


    /**
     * Получить значение одноразового счетчика
     * Может быть как счетчик, так и текущее время в секундах
     * @return int
     */
    public function getNonce();


    /**
     * Установить значение одноразового счетчика
     * При использовании текущего времени можно оставить заглушку
     * @param int $nonce
     * @return void
     */
    public function setNonce($nonce);


    /**
     * Получить тип одноразового счетчика
     * @return int
     */
    public function getNonceType();


    /**
     * Получить хранимый секрет
     * secret = sha1(pin . deviceId . halfSecret)
     * @return string
     */
    public function getSecret();


    /**
     * Установит секрет
     * secret = sha1(pin . deviceId . halfSecret)
     * @param string $secret
     * @return void
     */
    public function setSecret($secret);


    /**
     * Пометить пользователя как рассинхронизированного с ключом
     * @return void
     */
    public function markDesync();


    /**
     * Проверить является ли пользователь рассинхронизованным с ключом
     * @return boolean
     */
    public function isDesync();

}
