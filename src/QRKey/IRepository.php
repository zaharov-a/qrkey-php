<?php

namespace QRKey;

/**
 * Интерфейс репозитория пользователей
 * Interface IRepository
 * @package QRKey
 */
interface IRepository
{


    /**
     * Найти пользователя по логину
     * @param string $login
     * @return IUser|null
     */
    public function findByLogin($login);


    /**
     * Найти пользователя по токену
     * @param string $token
     * @return IUser|null
     */
    public function findByToken($token);

}
